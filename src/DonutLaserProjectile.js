import('/src/jsgame/Projectile.js');

/**
 * @class
 */
class DonutLaserProjectile extends Projectile {

    constructor() {
        super(...arguments);

        this.damage = 100;
        this.width = DonutLaserProjectile.WIDTH;
        this.height = DonutLaserProjectile.HEIGHT;
        this.hurtEnemies = false;
        this.hurtPlayer = true;
    }

    draw() {
        const graphics = this.game.graphics;

        graphics.stroke(255, 0, 0);
        graphics.noFill();

        graphics.ellipse(
            this.x + 0.5,
            this.y + 0.5,
            this.width,
            this.height,
        ); 
    }
    
}

DonutLaserProjectile.WIDTH = 8;
DonutLaserProjectile.HEIGHT = 8;
