import('/src/jsgame/Enemy.js');
import('DonutLaserProjectile.js');

/**
 * @class
 */
class MBEnemy extends Enemy {

    constructor() {
        super(...arguments);

        this.health = MBEnemy._MAX_HEALTH ;
        this.width = 24;
        this.height = 24;
        this.damage = 25;

        this._collideWithGroups = [];

        this._interval = setInterval(() => {
            this._fire();
        }, MBEnemy._FIRE_INTERVAL_MS);
    }

    reset() {
        this.health = MBEnemy._MAX_HEALTH ;
    }

    /**
     * @private
     */
    _fire() {
        let proj = new DonutLaserProjectile(
            this.x + this.width / 2, this.y + this.height / 2);

        const theta = Math.random() * Math.PI * 2;

        proj.vx = Math.cos(theta);
        proj.vy = Math.sin(theta);

        this.game.currentRoom.addDrawable(proj);        
    }

    tearDown() {
        clearInterval(this._interval);
    }

    draw() {
        const graphics = this.game.graphics;
        const x = this.x + 0.5;
        const y = this.y + 0.5;

        graphics.fill(255, 0, 0);
        graphics.noStroke();
        graphics.ellipse(x + this.width / 2, y + this.height / 2, this.width, this.height);  

        graphics.fill(255);
        graphics.ellipse(x + this.width / 2, y + this.height / 2, this.width / 2, this.height / 2);

        graphics.fill(0);

        let size = this.width / 4;
        if (this.game.getBeat() == 0) size += 1;

        graphics.ellipse(x + this.width / 2, y + this.height / 2, size, size);

        graphics.fill('#0000FF77');

        const h = this.height * this.health / MBEnemy._MAX_HEALTH;

        graphics.rect(
            x, y + this.height, this.width, -h);

        graphics.stroke(255);
        graphics.noFill();

        graphics.rect(
            x, y, this.width, this.height);

        super.draw(); 
    }
    
}

MBEnemy._MAX_HEALTH = 100;
MBEnemy._FIRE_INTERVAL_MS = 1000;
MBEnemy._WALK_SPEED = 0;
MBEnemy._GRAVITY = 0;
