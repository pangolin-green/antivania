
class Planner {

    static init(mb) {
        this._mb = mb;
    
        this.plan();
    }

    static plan() {
        
    }

    static getInstruction(x, y) {
        if (x < this._mb.x - 32) {
            return this.INSTRUCTION.FORWARD;
        } else if (x > this._mb.x + 32) {
            return this.INSTRUCTION.BACK;
        }

        return null;
    }

}

Planner.INSTRUCTION = {
    FORWARD: 0,
    BACK: 1,
    JUMP: 2,
};