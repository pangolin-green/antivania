
/**
 * @class
 */
class DoorProp extends Prop {

    constructor() {
        super(...arguments);

        this.width = DoorProp.WIDTH;
        this.height = DoorProp.HEIGHT;
        this.projectileCollisions = true;
        this.open = false;
        this._interval = null;
        this._color = '#FFFF00';
    }

    onPlayerCollision(player) {
        if (this.open) {
            player.x = this.x + DoorProp.WIDTH + BlockProp.WIDTH;
        }
    }

    onProjectileCollision(projectile) {
        this.open = true;

        clearInterval(this._interval);

        this._interval = setTimeout(() => {
            this.open = false;
        }, DoorProp._OPEN_INTERVAL_MS);
    }


    draw() {
        const graphics = this.game.graphics;

        graphics.fill(this._color);
        graphics.noStroke();

        if (this.open) {
            graphics.rect(this.x + 0.5, this.y + 0.5, DoorProp.WIDTH, DoorProp.HEIGHT / 8); 
            graphics.rect(
                this.x + 0.5, this.y + 0.5 + DoorProp.HEIGHT, DoorProp.WIDTH, -DoorProp.HEIGHT / 8); 

        } else {
            graphics.rect(this.x + 0.5, this.y + 0.5, DoorProp.WIDTH, DoorProp.HEIGHT); 
        }
    }
    
}

DoorProp.NAME = 'Blast Door';
DoorProp.COST = 100;

DoorProp.WIDTH = 8;
DoorProp.HEIGHT = 24;
DoorProp._OPEN_INTERVAL_MS = 1000;