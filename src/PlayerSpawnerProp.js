import('/src/jsgame/Prop.js');
import('EnemyPlayer.js');
import('upgrades/UpgradeSplashProp.js');

class PlayerSpawnerProp extends Prop {

    constructor() {
        super(...arguments);

        this.width = PlayerSpawnerProp.WIDTH;
        this.height = PlayerSpawnerProp.HEIGHT;

        this.wave = 1;
        this._spawnCount = 0;
        this._numPerWave = this.constructor._NUM_PER_WAVE;

        this._active = false;

        Mouse.onRelease((event) => {
            if (this.game.paused) return;
            
            this._handleMouseClick(event);
        });
    }

    get active() {
        return this._active;
    }

    _handleMouseClick(event) {
        const pos = this.game.getMousePosInGame();


        if (this.pointIsInside(pos.x, pos.y, this.hitbox)) {
            if (!this._active) {
                this._startWave();;
            }
        }
    }

    draw() {
        const graphics = this.game.graphics;

        if (this._active) {
            graphics.fill('#800080');
        } else {
            if (this.game.getBeat() == 0) {
                graphics.fill('#9932cc');
            } else {
                graphics.fill('#4b0082');
            }
        }
        graphics.noStroke();

        graphics.rect(this.x, this.y, this.width, this.height);

        if (!this._active) {
            graphics.fill(255);
            graphics.textAlign(Shapes.ALIGN.CENTER, Shapes.ALIGN.CENTER);
            graphics.text('Wave: ' + this.wave, this.x + this.width / 2, this.y + this.height / 2);
        }
    }

    update() {
        if (this._active && this.game.frame % this.constructor._INTERVAL_FRAMES === 0) {
            this.trySpawnPlayer();
        }

        if (this._active && this._spawnCount >= this._numPerWave) {
            // Check for wave over.
            let over = true;

            for (let drawable of this.game.currentRoom.props) {
                if (drawable instanceof Player) {
                    over = false;

                    break;
                }
            }

            if (over) {
                this._endWave();
            }
        }
    }

    spawnEnemies() {
        this.game.currentRoom.props.forEach((prop) => {
            if (prop instanceof EnemySpawnerProp) {
                prop.spawn();
            }
        });

    }

    _startWave() {
        // TODO: Clear held item.
        
        this._clearEnemies();
        this._clearPlayers();

        this.spawnEnemies();

        this.constructor._UPGRADES.forEach((upgrade) => {
            if (upgrade.wave === this.wave) {
                this._announceUpgrade(upgrade);
            }
        });

        this._active = true;
        this._spawnCount = 0;

        this.trySpawnPlayer();
    }

    _clearEnemies() {
        this.game.currentRoom.enemies.forEach((enemy) => {
            if (!(enemy instanceof MBEnemy)) {
                this.game.currentRoom.removeDrawable(enemy);
            } else {
                enemy.reset();
            }
        });
    }

    _clearPlayers() {
        this.game.currentRoom.props.forEach((prop) => {
            if (prop instanceof Player) {
                this.game.currentRoom.removeDrawable(prop);
            }
        });
    }

    _endWave() {
        this._active = false;

        this._clearEnemies();

        this.game.currentRoom.props.forEach((prop) => {
            if (prop instanceof EnemySpawnerProp) {
                prop.reset();
            }
        });

        this.wave++;
    }

    _createPlayer() {
        let p = new EnemyPlayer(this.x, this.y);

        p.game = Antivania;

        this.constructor._UPGRADES.forEach((upgrade) => {
            if (upgrade.wave <= this.wave) {
                upgrade.f(p);
            }
        });

        return p;
    }

    spawnControllablePlayer() {
        let p = this._createPlayer();
        p.initControls();

        this.game.currentRoom.addDrawable(p);
    }

    resetWave() {
        this._endWave();
        this._clearPlayers();

        this.wave--;
    }

    trySpawnPlayer() {
        if (this._spawnCount >= this._numPerWave) return;

        const p = this._createPlayer();

        this.game.currentRoom.addDrawable(p);

        this._spawnCount++;
    }

    _announceUpgrade(upgrade) {
        const prop = new UpgradeSplashProp(upgrade);

        this.game.currentRoom.addDrawable(prop);
    }

}

PlayerSpawnerProp._INTERVAL_FRAMES = 30 * 5;
PlayerSpawnerProp._NUM_PER_WAVE = 5;

PlayerSpawnerProp.WIDTH = 48;
PlayerSpawnerProp.HEIGHT = 16;

PlayerSpawnerProp._UPGRADES = [
    {
        wave: 3,
        name: 'Large Shot',
        description: 'A powerful charged shot',
        f: (player) => {
            player.hasLargeShot = true;
        }
    },
    {
        wave: 6,
        name: 'Spacer',
        description: 'Expands the beam',
        f: (player) => {
            player.hasSpacer = true;
        }
    },
    {
        wave: 12,
        name: 'Z-Tank',
        description: 'Increases health',
        f: (player) => {
            player.health += 100;
        }
    },
    {
        wave: 9,
        name: 'Rocket',
        description: 'A strong projectile',
        f: (player) => {
            player.hasMissile = true;
        }
    },
];