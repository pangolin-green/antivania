import('/src/JSUtils/util/Driver.js');
import('/src/JSUtils/util/Page.js');
import('Antivania.js');

/**
 * @class
 */
class HomepageViewDriver extends Driver {

    /**
     * @public
     */
    static init() {
        super.init();

        Antivania.init(ID.CANVAS);
    }
}

Page.addLoadEvent(() => {
    HomepageViewDriver.init();
});

const ID = {
    CANVAS: 'canvas',
    TEMPLATE: {
        
    }
};

const CLASS = {

};
