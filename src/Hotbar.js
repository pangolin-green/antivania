import('/src/JSUtils/io/Mouse.js');
import('/src/jsgame/ConfigurableController.js');
import('/src/jsgame/Drawable.js');
import('EnemySpawnerProp.js');

/**
 * @class
 */
class Hotbar extends Drawable {

    constructor() {
        super(...arguments);

        this.width = 0;
        this.height = Hotbar._SIZE;

        this.credit = this.constructor._DEFAULT_CREDIT;

        this._index = 0;
        this._drawables = [];
        this._drawableClasses = [];
        this._held = null;

        Mouse.onRelease((event) => {
            this._handleMouseClick(event);
        });
    }

    addDrawable(drawableClass) {
        let instance = new drawableClass(
            this.x + Hotbar._SIZE / 2 + this._drawables.length * Hotbar._SIZE,
            this.y + Hotbar._SIZE / 2,
        );

        instance.x -= instance.width / 2;
        instance.y -= instance.height / 2;
        instance.vx = 0;
        instance.vy = 0;

        this._drawables.push(instance);
        instance.game = Antivania;

        this._drawableClasses.push(drawableClass);

        this.width += Hotbar._SIZE;

        return this;
    }

    _copy(x) {
        if (x.copy) return x.copy();

        return new (x.constructor)(x.x, x.y);
    }

    _setHeld(held) {
        if (this._held) {
            this.game.currentRoom.removeDrawable(this._held);
        }

        if (held) {
            held = this._copy(held);

            if (held instanceof Enemy) {
                held = new EnemySpawnerProp(held.x, held.y)
                    .setEnemy(held);
            }

            this._held = held;

            this.game.currentRoom.addDrawable(this._held);
        } else {
            this._held = null;
        }
    }

    _handleMouseClick(event) {
        const pos = this.game.getMousePosInGame();

        if (!this.game.canEditRoom()) return;

        if (Mouse.isRightClick(event)) {
            if (this._held) {
                this._setHeld(null);
            } else {
                const d = this.game.currentRoom.getDrawableAtPoint(pos.x, pos.y);
            
                if (d) {
                    this.game.currentRoom.removeDrawable(d);

                    this.credit += d.cost || d.constructor.COST || 0;
                }
            }
        }

        if (this.pointIsInside(pos.x, pos.y, this.hitbox)) {
            this._index = Math.floor((pos.x - this.x) / Hotbar._SIZE);
            const instance = this._drawables[this._index];

            this._setHeld(instance);
        } else if (this._held) {
            this._tryPlaceDrawable();
        }
    }

    _tryPlaceDrawable() {
        if (this.credit < this._drawableClasses[this._index].COST) return;
        
        this.credit -= this._drawableClasses[this._index].COST;

        let instance = this._copy(this._held);

        this.game.currentRoom.addDrawable(instance);
    }

    /**
     * @public
     */
    update() {
        const pos = this.game.getMousePosInGame();

        if (this._held) {
            this._held.x = Math.floor(pos.x / 8) * 8;
            this._held.y = Math.floor(pos.y / 8) * 8;
        }
    }

    /**
     * @public
     */
    draw() {
        const graphics = this.game.graphics;

        graphics.noFill();

        for (let i = 0; i < this._drawables.length; i++) {
            if (i === this._index) {
                graphics.stroke(255);
            } else {
                graphics.stroke(200);
            }

            graphics.noFill();
            
            graphics.rect(
                this.x + 0.5 + Hotbar._SIZE * i,
                this.y + 0.5,
                Hotbar._SIZE,
                Hotbar._SIZE,
            );

            const drawable = this._drawables[i];
            const ratio = (Hotbar._SIZE / 2) / drawable.height;
            // const ratio = 0.7;

            graphics.push();
            graphics.translate(
                drawable.x + drawable.width / 2, drawable.y + drawable.height / 2);
            graphics.scale(ratio);
            graphics.translate(
                -(drawable.x + drawable.width / 2), -(drawable.y + drawable.height / 2));

            drawable.draw();

            graphics.pop();

        }

        graphics.fill(255);
        graphics.noStroke();

        const cls = this._drawableClasses[this._index];
        const s = cls.NAME + ' (' + cls.COST + 'c)'; 

        graphics.textAlign(Shapes.ALIGN.LEFT);
        graphics.text(s, this.x, this.y - 8);
        graphics.text('Credit: ' + this.credit, this.x, this.y - 20);
    }
}

Hotbar._SIZE = 16;
Hotbar._DEFAULT_CREDIT = 600;