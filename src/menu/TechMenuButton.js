import('/src/jsgame/menu/MenuButton.js');

class TechMenuButton extends MenuButton {

    constructor() {
        super(...arguments);

        this.width = 72;
        this.height = 48;

        this._unlocked = false;
        this._cost = 0;
        this._dependencies = [];
        this._onUnlock = null;
        this._bgColor = '#f08080';

        this.setOnClick(() => {
            this._tryUnlock();
        });
    }

    _tryUnlock() {
        if (this._unlocked) return;

        for (let dependency of this._dependencies) {
            if (!dependency._unlocked) {
                return;
            }
        }

        this._unlocked = true;
        this._bgColor = '#74c365';

        if (this._onUnlock) this._onUnlock();
    }

    setOnUnlock(f) {
        this._onUnlock = f;

        return this;
    }

    setDependencies(dependencies) {
        this._dependencies = dependencies;

        return this;
    }

    setCost(cost) {
        this._cost = cost;

        return this;
    }

    get dependencies() {
        return this._dependencies.slice();
    }

    getDependencyDepth() {
        let result = 0;
        let queue = [[this, 0]];
        let buttonsSeen = new Set();

        while (queue.length > 0) {
            const item = queue.pop();
            const button = item[0];
            const depth = item[1];

            result = Math.max(result, depth);

            if (buttonsSeen.has(button)) continue;

            buttonsSeen.add(button);

            for (let dependency of button.dependencies) {
                queue.push([dependency, depth + 1]);
            }
        }

        return result;
    }

    getMaxDependencyWidth() {
        // ...
    }
}