import('/src/jsgame/menu/Menu.js');
import('/src/jsgame/menu/MenuButton.js');

/**
 * @class
 */
class ControlsMenu extends Menu {

    constructor() {
        super(...arguments);

        this.x = Antivania.viewWidth / 2;
        this.y = Antivania.viewHeight / 2;

        this.addButton(new MenuButton('Run Mode', 0, 0, 64)
            .setOnClick(() => {
                this.game.spawnControllablePlayer();
                this.game.unpause();
            }));

        this.addButton(new MenuButton('Reset Wave', 0, 36, 64)
            .setOnClick(() => {
                this.game.resetWave();
                this.game.unpause();
            }));

        this.addButton(new MenuButton('Resume', 0, 72, 64)
            .setOnClick(() => {
                this.game.unpause();
            }));
    }

    draw() {
        const graphics = this.game.graphics;

        graphics.background(0);

        super.draw();
    }
}