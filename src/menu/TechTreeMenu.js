import('/src/jsgame/menu/Menu.js');
import('../blocks/PitfallBlockProp.js');
import('../blocks/RocketBlockProp.js');
import('../enemies/ZipperEnemy.js');
import('TechMenuButton.js');

/**
 * @class
 */
class TechTreeMenu extends Menu {

    constructor() {
        super(...arguments);

        this.x = Antivania.viewWidth / 2;
        this.y = Antivania.viewHeight / 2;
        this._buttonsUnlocked = new Set();
        this._numButtonsByDepth = [];

        const pitfall = new TechMenuButton('Pitfall Block', 0, 0, 64)
            .setCost(1)
            .setOnUnlock(() => {
                this.game.addTech(PitfallBlockProp);
            });

        this.addButton(pitfall);

        const rocketBlock = new TechMenuButton('Rocket Block', 0, 0)
            .setCost(1)
            .setDependencies([pitfall])
            .setOnUnlock(() => {
                this.game.addTech(RocketBlockProp);
            });

        this.addButton(rocketBlock);

        const rocketDoor = new TechMenuButton('Rocket Door', 0, 0)
            .setCost(1)
            .setOnUnlock(() => {
                console.log('rocket door');
            });

        this.addButton(rocketDoor);

        const superRocketDoor = new TechMenuButton('Super Rocket Door', 0, 0)
            .setCost(1)
            .setDependencies([rocketDoor])
            .setOnUnlock(() => {
                console.log('super rocket door');
            });

        this.addButton(superRocketDoor);

        const zipperEnemy = new TechMenuButton('Zipper Enemy', 0, 0)
            .setCost(1)
            .setOnUnlock(() => {
                this.game.addTech(ZipperEnemy);
            });

        this.addButton(zipperEnemy);

        const gliderEnemy = new TechMenuButton('Glider Enemy', 0, 0)
            .setCost(1)
            .setDependencies([zipperEnemy])
            .setOnUnlock(() => {
                console.log('glider');
            });

        this.addButton(gliderEnemy);

        const swarmEnemy = new TechMenuButton('Swarm Enemy', 0, 0)
            .setCost(1)
            .setDependencies([zipperEnemy])
            .setOnUnlock(() => {
                console.log('swarm');
            });

        this.addButton(swarmEnemy);
    }

    /**
     * @public
     * @override
     */
    addButton(button, cost) {
        super.addButton(button, 0);

        // TODO.
        const depth = button.getDependencyDepth();

        if (!this._numButtonsByDepth[depth]) {
            this._numButtonsByDepth[depth] = 1;
        } else {
            this._numButtonsByDepth[depth]++;
        }

        const numButtons = this._numButtonsByDepth[depth];

        button.x = -this.x + (button.width + 16) * numButtons;
        button.y = depth * (button.height + 16);
    }

    /**
     * @public
     * @override
     */
    draw() {
        const graphics = this.game.graphics;

        graphics.background(0);

        // Draw dependencies.
        graphics.stroke(255);
        graphics.noFill();

        graphics.push();
        graphics.translate(this.x, this.y);

        for (let i in this._buttons[0]) {
            const button = this._buttons[0][i];

            for (let button2 of button.dependencies) {
                const x1 = button.x;
                const y1 = button.y;
                const x2 = button2.x;
                const y2 = button2.y;
                const width = x2 - x1;
                const height = y2 - y1;

                graphics.line(x1, y1, x1, y1 + height / 2);
                graphics.line(x1, y1 + height / 2, x1 + width, y1 + height / 2);
                graphics.line(x1 + width, y1 + height / 2, x2, y2);
            }
        }

        graphics.pop();

        super.draw();
    }
}