
class EnemySpawnerProp extends Prop {

    constructor() {
        super(...arguments);

        this._enemy = null;
        this._draw = true;
    }

    setEnemy(enemy) {
        this._enemy = this._copy(enemy);
        this._enemy.game = Antivania;

        return this;
    }

    get cost() {
        return this._enemy.constructor.COST;
    }

	_copy(x) {
        return new (x.constructor)(x.x, x.y);
    }

    copy() {
        return new EnemySpawnerProp(this.x, this.y)
            .setEnemy(this._enemy);
    }

    update() {
        this._enemy.x = this.x;
        this._enemy.y = this.y;
    }

    draw() {
        if (!this._draw) return;

        // this.game.graphics._context.globalAlpha = 0.5;
        this._enemy.draw();
        // this.game.graphics._context.globalAlpha = 1;
    }

    reset() {
        this._draw = true;
    }

    spawn() {
        const instance = this._copy(this._enemy);

        this.game.currentRoom.addDrawable(instance);

        this._draw = false;
    }
}