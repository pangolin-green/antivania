import('/src/JSUtils/math/Random.js');
import('/src/jsgame/SidescrollShooterPlayer.js');
import('projectiles/BeamProjectile.js');
import('projectiles/RocketProjectile.js');

/**
 * @class
 */
class EnemyPlayer extends SidescrollShooterPlayer {

    constructor() {
        super(...arguments);

        this.width = 8;
        this.height = 16;
        this._vx = this.constructor._RUN_SPEED;
        this._ai = true;
        this._aimTheta = 0;
        this._missileArmed = false;

        this.hasMachBall = false;
        this.hasLargeShot = false;
        this.hasSpacer = false;
        this.hasMissile = false;

        this.collisionGroups.push(Antivania.COLLISION.PLAYERS);

        const colorSet = Random.choice(this.constructor._COLORS);

        this._colorTop = colorSet[0];
        this._colorBody = colorSet[1];
        this._colorVisor = colorSet[2];
        this._limbsColor = colorSet[3];
    }

    _makeProjectile(cls) {
        let p = new (cls)(
            this.x + this.width / 2, this.y + this.height / 3);

        if (this._ai) {
            if (Math.random() < this.constructor._AIM_P) {
                this._aimTheta = Random.choice([
                    0, -Math.PI/4, -Math.PI/2, Math.PI/4, Math.PI/2]);
            }
        } else {
            this._aimTheta = this.getAimTheta();
        }

        p.vx = 3 * Math.cos(this._aimTheta) * (Math.sign(this.vx) || 1);
        p.vy = 3 * Math.sin(this._aimTheta);

        return p;
    }

    _makeProjectiles() {
        let result = [];

        if (Math.random() < this.constructor._ARM_MISSILE_P) {
            this._missileArmed = !this._missileArmed;
        }

        if (this.hasMissile && this._missileArmed) {
            result.push(this._makeProjectile(RocketProjectile));

        } else {
            let p = this._makeProjectile(BeamProjectile);

            if (this.hasLargeShot) {
                p.width += 2;
                p.height += 2;
                p.damage += 2;
            }

            result.push(p);

            if (this.hasSpacer) {
                const dir = new Vector(p.vx, p.vy).normalize();
                const up = dir.rotateZ(-Math.PI / 2).multiply(6);
                const down = dir.rotateZ(Math.PI / 2).multiply(6);

                let top = p.copy();
                top.x += up.x;
                top.y += up.y;

                result.push(top);

                let bottom = p.copy();
                bottom.x += down.x;
                bottom.y += down.y;

                result.push(bottom);
            }
        }

        return result;
    }

    _doAI() {
        if (this._vx === 0) {
            this._vx = 1;
        }

        const clear = this._positionIsClear(this._vx, 0, this, this._collideWithGroups);

        if (!clear) {
            if (this._positionIsClear(this._vx, -16, this, this._collideWithGroups)) {
                this._jump();
            } else {
                this._vx *= -1;
            }
        }

        if (this._canJump()) {
            if (Math.random() < this.constructor._JUMP_PROBABILITY) {
                this._jump();
            }
        }

        this._fire();
    }

    initControls() {
        super.initControls();

        this._ai = false;
        this._vx = 0;
    }

    update() {
        super.update();

        if (this._ai) {
            this._doAI();
        }

        // TODO: Check for upgrades.
    }

    draw() {
        const graphics = this.game.graphics;
        let x = this.x + 0.5;
        const beat = this.game.getBeat()
        let y = this.y + 0.5;

        graphics.noStroke();

        graphics.fill(this._colorBody);

        const legY = y + Math.floor(this.height * 0.75);
        let off1 = 0;
        let off2 = 0;

        if (beat === 0) {
            off1 -= 2;
        } else {
            off2 -= 2;
        }

        // Body.
        graphics.fill(this._colorBody);
        graphics.rect(x, y + this.height / 4, this.width, this.height / 2);

        // Arms.
        graphics.fill(this._limbsColor);
        graphics.rect(x - 4, y + this.height / 4 + off2, 4, 4);
        graphics.rect(x + this.width, y + this.height / 4 + off1, 4, 4);

        // Legs.
        graphics.rect(x, legY, this.width / 2 + 1, this.height / 4 + off1);
        graphics.rect(x - 1 + this.width / 2, legY, this.width / 2 + 1, this.height / 4 + off2);


        if (beat === 0) {
            x -= 1;
        } else {
            x += 1;
        }

        // Head.
        graphics.fill(this._colorTop);
        graphics.rect(x, y, this.width, this.width);

        graphics.fill(this._colorVisor);
        graphics.rect(x, y + this.height / 8, this.width, 4);
    }

    _land() {
        super._land();

        this._checkVelocity();
    }

    /**
     * @private
     */
    _onHurt() {
        if (this.health <= 0) {
            this.game.currentRoom.removeDrawable(this);

            this.game.onPlayerKilled();
        }
    }

    _onLoseInvulnerability() {
        this._checkVelocity();
    }

    _checkVelocity() {
        if (this._vx < -this.constructor._RUN_SPEED) {
            this._vx = -this.constructor._RUN_SPEED;
        } else if (this._vx > this.constructor._RUN_SPEED) {
            this._vx = this.constructor._RUN_SPEED;
        }
    }
}

EnemyPlayer._AIM_P = 0.2;
EnemyPlayer._ARM_MISSILE_P = 0.1;
EnemyPlayer._RUN_SPEED = 1;
EnemyPlayer._JUMP_VELOCITY = 3.2;
EnemyPlayer._JUMP_PROBABILITY = 0.02;
EnemyPlayer._FIRE_INTERVAL_MS = 300;
EnemyPlayer._IMPACT_VELOCITY = 1;
EnemyPlayer._FIRE_DELAY_MS = 100;

EnemyPlayer._COLORS = [
    ['#FF0000', '#ff8c00', '#00ff00', '#FF0000'],
    ['#FF0000', '#fff8dc', '#00ff00', '#1e90ff'],
    ['#f8f8ff', '#f8f8ff', '#ffa500', '#333333'],
];
