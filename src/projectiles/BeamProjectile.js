import('/src/jsgame/Projectile.js');

/**
 * @class
 */
class BeamProjectile extends Projectile {

    constructor() {
        super(...arguments);

        this.damage = this.constructor.DAMAGE;
        this.width = this.constructor.WIDTH;
        this.height = this.constructor.HEIGHT;
    }

    draw() {
        const graphics = this.game.graphics;

        graphics.fill(200, 200, 0);
        graphics.noStroke();

        graphics.rect(
            this.x - this.width / 2 + 0.5,
            this.y - this.height / 2 + 0.5,
            this.width,
            this.height,
        ); 
    }
    
}

BeamProjectile.DAMAGE = 1;
BeamProjectile.WIDTH = 4;
BeamProjectile.HEIGHT = 4;
