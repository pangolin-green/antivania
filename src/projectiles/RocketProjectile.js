import('BeamProjectile.js');

/**
 * @class
 */
class RocketProjectile extends BeamProjectile {

    draw() {
        const graphics = this.game.graphics;

        graphics.fill(200, 0, 0);
        graphics.noStroke();

        graphics.rect(
            this.x - this.width / 2 + 0.5,
            this.y - this.height / 2 + 0.5,
            this.width,
            this.height,
        ); 
    }
    
}

RocketProjectile.DAMAGE = 5;
RocketProjectile.WIDTH = 5;
RocketProjectile.HEIGHT = 5;
