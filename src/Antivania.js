import('/src/jsgame/CollisionGroup.js');
import('/src/jsgame/Game.js');
import('blocks/BeamBlockProp.js');
import('DoorProp.js');
import('enemies/GrooverEnemy.js');
import('Hotbar.js');
import('MBEnemy.js');
import('menu/ControlsMenu.js');
import('menu/TechTreeMenu.js');
import('Planner.js');
import('PlayerSpawnerProp.js');
import('RoomProp.js');

/**
 * @class
 */
class Antivania extends Game {

    static _initRooms() {

        this._playerSpawner = new PlayerSpawnerProp(28, 28);
        this.currentRoom.addProp(this._playerSpawner);

        for (let i = 0; i < 3; i++) {
            const x = i * (RoomProp.WIDTH);

            this.currentRoom.addProp(new RoomProp(x, 0));

            if (i !== 2) {
                this.currentRoom.addProp(
                    new DoorProp(
                        x + RoomProp.WIDTH - DoorProp.WIDTH,
                        RoomProp.HEIGHT - DoorProp.HEIGHT), 1);
            }

            if (i == 2) {
                const mb = new MBEnemy(x + 8 * 10, RoomProp.HEIGHT / 2);
                
                this.currentRoom.addProp(mb);
            
                Planner.init(mb);
            }
        }

    }

    static init() {
        super.init(...arguments);

        // Start preloading.
        JSAudio.load(this._BG_MUSIC_URI);

        Mouse.onRelease(() => {
            this._tryStart();
        });

        this._techTree = new TechTreeMenu();

        this._menu = new ControlsMenu();

        ConfigurableController
            .setInput(
                this.INPUT.PAUSE,
                new KeyDownBooleanInput(KEYS.ESCAPE_KEY),
            )
            .setInput(
                this.INPUT.RESEARCH,
                new KeyDownBooleanInput(KEYS.R_KEY),
            );

        ConfigurableController.onInputReleased(this.INPUT.PAUSE, () => {
            this.togglePause(this._menu);
        });

        ConfigurableController.onInputReleased(this.INPUT.RESEARCH, () => {
            this.togglePause(this._techTree);
        });
        
    }

    static _tryStart() {
        if (this._started) return;

        this._started = true;

        this.currentRoom.setMusic(this._BG_MUSIC_URI);

        this.currentRoom.load().then(() => {

            this._initRooms();

            this._hotbar = new Hotbar(20, 300)
                .addDrawable(GrooverEnemy)
                .addDrawable(BeamBlockProp)
                .addDrawable(DoorProp)
                .addDrawable(BlockProp)
            ;

            this.currentRoom.addDrawable(this._hotbar);
        });
    }

    static addTech(drawable) {
        this._hotbar.addDrawable(drawable);
    }

    static draw() {
        const graphics = this.graphics;
        const pulse = this.getBeatReal();

        graphics.background(0);

        if (!this._started) {
            graphics.fill(255);
            graphics.noStroke();

            graphics.textAlign(Shapes.ALIGN.CENTER, Shapes.ALIGN.CENTER);

            graphics.textSize(24);
            graphics.text('Antivania', this._VIEW_WIDTH / 2, this._VIEW_HEIGHT / 2);
            
            graphics.textSize(12);
            graphics.text('An open-source game for charity', this._VIEW_WIDTH / 2, this._VIEW_HEIGHT / 2 + 32);
            graphics.text('By The Pangolin Green Foundation', this._VIEW_WIDTH / 2, this._VIEW_HEIGHT / 2 + 48);

            // https://gitlab.com/pangolin-green/antivania
        }

        super.draw();

        if (this._paused) {
            this._activePauseMenu.draw();
        }

        this.frame++;
    }

    static onPlayerKilled() {
        this._hotbar.credit += 100;
    }

    static getBeat() {
        const t = this.currentRoom.musicAudio.currentTime;
        const beats = t / 60 * this._BPM;

        return Math.floor(beats) % 2;
    }

    static getBeatReal() {
        if (!this.currentRoom.musicAudio) return 0;

        const t = this.currentRoom.musicAudio.currentTime;
        const beats = t / 60 * this._BPM;
        const beatPart = beats % 1;

        return (Math.sin(beatPart * Math.PI) + 1) / 2;
    }

    static spawnControllablePlayer() {
        this._playerSpawner.spawnControllablePlayer();
        this._playerSpawner.spawnEnemies();
    }

    static resetWave() {
        this._playerSpawner.resetWave();
    }

    static canEditRoom() {
        return !this._playerSpawner.active;
    }
}

Antivania.COLLISION.PLAYERS = new CollisionGroup();
Antivania.COLLISION.UPGRADES = new CollisionGroup();

Antivania.frame = 0;

Antivania._BPM = 126;
Antivania._BG_MUSIC_URI = 'edm-detection-mode.mp3';

Antivania._started = false;

Antivania.INPUT = {
    PAUSE: 'pause',
    RESEARCH: 'research',
};