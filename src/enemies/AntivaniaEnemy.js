import('/src/jsgame/Enemy.js');

class AntivaniaEnemy extends Enemy {

    get cost() {
        return this.constructor.COST;
    }
}

AntivaniaEnemy.COST = 100;