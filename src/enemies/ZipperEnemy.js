import('AntivaniaEnemy.js');

/**
 * @class
 */
class ZipperEnemy extends AntivaniaEnemy {

    constructor() {
        super(...arguments);

        this.health = this.constructor._MAX_HEALTH;
        this.damage = 25;

        this.width = 8;
        this.height = 8;
    }

    draw() {
        const graphics = this.game.graphics;
        const x = this.x + 0.5;
        const y = this.y + 0.5;

        graphics.fill('#a0522d');
        graphics.noStroke();

        graphics.rect(
            x, y, this.width, this.height);

        super.draw(); 
    }
    
}

ZipperEnemy.NAME = 'Zipper';
ZipperEnemy.COST = 300;

ZipperEnemy._MAX_HEALTH = 10;
ZipperEnemy._WALK_SPEED = 1;
ZipperEnemy._GRAVITY = 0;
