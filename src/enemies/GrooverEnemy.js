import('AntivaniaEnemy.js');

/**
 * @class
 */
class GrooverEnemy extends AntivaniaEnemy {

    constructor() {
        super(...arguments);

        this.health = GrooverEnemy._MAX_HEALTH;
        this.damage = 15;

        this.width = 8;
        this.height = 8;

        this.active = false;

        this._v = new Vector(this._vx, this._vy).normalize();
        this._vPerp = this._v.rotateZ(Math.PI / 2);
        this._lastTurnMs = 0;
    }

    /**
     * @private
     */
    _forwardClear() {
        const xFront = this._v.x * this.constructor._WALK_SPEED;
        const yFront = this._v.y * this.constructor._WALK_SPEED;

        return this._positionIsClear(xFront, yFront);
    }

    /**
     * @private
     */
    _canTurnRight() {
        if ((new Date().getTime() - this._lastTurnMs) < this.constructor._TURN_DELAY_MS)
            return false;

        const xRight = this._vPerp.x * 8;
        const yRight = this._vPerp.y * 8;
        const x2 = xRight - this._v.x;
        const y2 = yRight - this._v.y;
        const rightClear = this._positionIsClear(xRight, yRight);
        const surface = !this._forwardClear() || !this._positionIsClear(x2, y2);

        return rightClear && surface;
    }

    /**
     * @private
     */
    _canTurnLeft() {
        if ((new Date().getTime() - this._lastTurnMs) < this.constructor._TURN_DELAY_MS)
            return false;

        const xLeft = -this._vPerp.x * 8;
        const yLeft = -this._vPerp.y * 8;
        const x2 = xLeft - this._v.x;
        const y2 = yLeft - this._v.y;
        const leftClear = this._positionIsClear(xLeft, yLeft);
        const surface = !this._forwardClear() || !this._positionIsClear(x2, y2);

        return leftClear && surface;
    }

    /**
     * @private
     */
    _turn(dTheta) {
        this._v = this._v.rotateZ(dTheta);
        this._vPerp = this._v.rotateZ(Math.PI / 2);
        this._lastTurnMs = new Date().getTime();
    }

    /**
     * @public
     */
    _clinging() {
        const pad = 1;

        return !this.game.currentRoom.rectIsClear(
            this._x - pad,
            this._y - pad,
            this.width + pad * 2,
            this.height + pad * 2,
            this,
            this._collideWithGroups,
        );
    }

    /**
     * @public
     * @override
     */
    update() {
        if (this._clinging()) {
            this._x += this._v.x * this.constructor._WALK_SPEED;
            this._y += this._v.y * this.constructor._WALK_SPEED;
        } else {
            // Do physics.
            super.update();
        }

        const turns = [
            {
                f: () => this._canTurnRight(),
                theta: Math.PI / 2,
            },
            {
                f: () => this._canTurnLeft(),
                theta: -Math.PI / 2,
            },
        ];
        const randTurns = Random.shuffle(turns);

        if (randTurns[0].f()) {
            this._turn(randTurns[0].theta);
        } else if (randTurns[1].f()) {
            this._turn(randTurns[1].theta);
        } else if (!this._forwardClear()) {
            this._turn(Math.PI);
        }
    }

    draw() {
        const graphics = this.game.graphics;
        const x = this.x + 0.5;
        const y = this.y + 0.5;

        graphics.fill('#ff69b4');
        graphics.noStroke();

        graphics.rect(
            x, y, this.width, this.height);

        graphics.fill(255);

        super.draw(); 
    }
    
}

GrooverEnemy.NAME = 'Groover';
GrooverEnemy.COST = 300;

GrooverEnemy._MAX_HEALTH = 3;
GrooverEnemy._WALK_SPEED = 0.5;
GrooverEnemy._TURN_DELAY_MS = 500;
GrooverEnemy._GRAVITY = 0.1;
