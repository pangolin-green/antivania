import('/src/jsgame/props/TileProp.js');

class BlockProp extends TileProp {

    constructor() {
        super(...arguments);

        this.width = BlockProp.WIDTH;
        this.height = BlockProp.WIDTH;
    }

    onProjectileCollision(projectile) {
        projectile.kill();
    }
    
    /**
     * @public
     */
    isSolid() {
        return true;
    }

    draw() {
        const graphics = this.game.graphics;

        graphics.fill(0, 0, 100);
        graphics.stroke(255);
        graphics.rect(this.x + 0.5, this.y + 0.5, this.width, this.height);
    }
}

BlockProp.WIDTH = 8;
BlockProp.NAME = 'Block';
BlockProp.COST = 0;