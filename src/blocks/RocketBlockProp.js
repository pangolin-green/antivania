import('BeamBlockProp.js');
import('../projectiles/RocketProjectile.js');

class RocketBlockProp extends BeamBlockProp {

    onProjectileCollision(projectile) {
        if (!this.isSolid()) return;

        projectile.kill();

        if (projectile instanceof RocketProjectile) {
            this._break();
        }
    }
}

RocketBlockProp.NAME = 'Rocket Block';
RocketBlockProp._COLOR = '#8b0000';
