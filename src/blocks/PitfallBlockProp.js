import('BeamBlockProp.js');

class PitfallBlockProp extends BeamBlockProp {

    onProjectileCollision(projectile) {
        projectile.kill();
    }

    /**
     * @private
     */
    _positionIsClear() {
        const room = this.game.currentRoom;

        if (!room) return false;

        return room.rectIsClear(
            this.x,
            this.y - 8,
            this.width,
            this.height,
            this,
            [
                Antivania.COLLISION.PLAYERS,
            ],
        );
    }

    update() {
        if (!this._positionIsClear()) {
            this._break();
        }

        super.update();
    }
}

PitfallBlockProp.NAME = 'Pitfall Block';
PitfallBlockProp._COLOR = '#AAFFAA';
