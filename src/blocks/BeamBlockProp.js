import('BlockProp.js');

class BeamBlockProp extends BlockProp {

    constructor() {
        super(...arguments);

        this._solid = true;
        this._interval = null;
    }

    /**
     * @override
     */
    isSolid() {
        return this._solid;
    }

    onProjectileCollision(projectile) {
        if (!this.isSolid()) return;

        super.onProjectileCollision(projectile);

        this._break();
    }

    /**
     * @private
     */
    _positionIsClear() {
        const room = this.game.currentRoom;

        if (!room) return false;

        return room.rectIsClear(
            this.x,
            this.y,
            this.width,
            this.height,
            this,
            [
                Antivania.COLLISION.PLAYERS,
            ],
        );
    }

    _break() {
        this._solid = false;

        this._interval = setInterval(() => {
            this._tryRegen()
        }, BeamBlockProp._REGEN_INTERVAL_MS);
    }

    _tryRegen() {
        if (this._positionIsClear()) {
            this._solid = true;
            clearInterval(this._interval);    
        }
    }
    
    draw() {

        const graphics = this.game.graphics;

        if (this._solid) {
           graphics.fill(this.constructor._COLOR);
           graphics.stroke(255); 
        } else {
            graphics.fill(this.constructor._COLOR + '88');
            graphics.noStroke();
        }

        
        graphics.rect(this.x + 0.5, this.y + 0.5, this.width, this.height);
    }
}

BeamBlockProp.NAME = 'Beam Block';
BeamBlockProp.COST = 100;
BeamBlockProp._COLOR = '#AAAAAA'
BeamBlockProp._REGEN_INTERVAL_MS = 2 * 1000;