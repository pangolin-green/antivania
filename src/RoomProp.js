import('blocks/BlockProp.js');

/**
 * @class
 */
class RoomProp extends Prop {

    constructor() {
        super(...arguments);

        this._tiles = [];
        this._instructions = null;
    }

    _tryPutTile(x, y) {
        const clear = this.game.currentRoom.rectIsClear(
            x, y, 8, 8, null, [
                Antivania.COLLISION.TILES,
            ],
        );

        if (clear) {
            const tile = new BlockProp(x, y);

            this._tiles.push(tile);
            this.game.currentRoom.addProp(tile);
        }
    }

    /**
     * @public
     */
    setup() {

        let x = this.x;
        let y = this.y + RoomProp.HEIGHT;

        while (x < this.x + RoomProp.WIDTH) {
            this._tryPutTile(x, y);
            this._tryPutTile(x, this.y);

            x += BlockProp.WIDTH;
        }

        x = this.x + RoomProp.WIDTH;
        y = this.y + RoomProp.HEIGHT;

        while (y >= this.y) {
            this._tryPutTile(x, y);

            y -= BlockProp.WIDTH;
        }

        x = this.x;
        y = this.y + RoomProp.HEIGHT;

        while (y >= this.y) {
            this._tryPutTile(x, y);

            y -= BlockProp.WIDTH;
        }
    }

    /**
     * @public
     */
    tearDown() {

    }

    draw() {
        // const graphics = this.game.graphics;

        // graphics.stroke(255);
        // graphics.noFill();
        // graphics.rect(this.x + 0.5, this.y + 0.5, RoomProp.WIDTH, RoomProp.HEIGHT);   
    }

}

RoomProp.WIDTH = 8 * 20;
RoomProp.HEIGHT = 8 * 12;
