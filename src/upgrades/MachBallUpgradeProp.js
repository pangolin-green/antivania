/*
 * @class
 */
class MachBallUpgradeProp extends Prop {

    constructor() {
        super(...arguments);
        this.width = 8;
        this.height = 8;
    }

    draw() {
        const graphics = this.game.graphics;

        graphics.fill(255, 0, 0);
        graphics.stroke(255, 255, 0);

        graphics.ellipse(this.x + this.width / 2, this.y + this.height / 2, this.width, this.height);
    }

}

MachBallUpgradeProp.NAME = 'Mach Ball';
MachBallUpgradeProp.COST = 1000;
