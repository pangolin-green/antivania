
class UpgradeSplashProp extends Prop {

    constructor(upgrade) {
        super();

        this._upgrade = upgrade;
    }

    setup() {
        setTimeout(() => {
            this.game.currentRoom.removeDrawable(this);
        }, this.constructor._LIFETIME_MS);
    }

    draw() {
        const graphics = this.game.graphics;

        graphics.fill(255);
        graphics.noStroke();

        const text = `${this._upgrade.name} aquired!`;

        graphics.textAlign(Shapes.ALIGN.CENTER, Shapes.ALIGN.CENTER);

        graphics.textSize(24);
        graphics.text(text, this.game.viewWidth / 2, this.game.viewHeight / 2);

        graphics.textSize(12);
        graphics.text(
            this._upgrade.description, this.game.viewWidth / 2, this.game.viewHeight / 2 + 32);
    }
}

UpgradeSplashProp._LIFETIME_MS = 3 * 1000;
