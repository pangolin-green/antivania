#!/usr/bin/env python3

from setuptools import setup, find_packages

setup(name='pythonapp',
    version='1.0',
    description='',
    author='John D. Sutton',
    author_email='',
    url='',
    packages=find_packages(),
    install_requires=[
        'nose2',
        'sphinx',
        'iso8601',
        'pythonlambdaserver @ git+https://gitlab.com/jdsutton/PythonLambdaServer',
        # 'eekquery @ git+https://gitlab.com/jdsutton/EekQuery',
        'slurp @ git+https://gitlab.com/jdsutton/Slurp',
        # 'py3typing @ git+https://gitlab.com/jdsutton/py3typing',
        # 'MockUtils @ git+https://gitlab.com/jdsutton/MockUtils',
    ],
)
