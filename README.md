# Antivania
An open-source game for charity by The Pangolin Green Foundation.

Donations support our mission to create a sustainable world: https://pangolin.green/donate/

## Getting Started

### Installation
```
git clone https://gitlab.com/pangolin-green/antivania.git
cd antivania

sudo apt update
sudo apt install virtualenv build-essential

# Make sure python3.6 is installed on your system:
python3 --version

virtualenv -p python3.6 env
. env/bin/activate

make install
```

### Run the webserver
```
# Make sure you have done . env/bin/activate first

make
```

Now you may open the page at http://0.0.0.0:50000